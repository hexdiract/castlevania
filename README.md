Castlevania
=======
This is a mod inspired by the game Castlevania. 


## Content
Blocks:
* Candles - Candles that drop items and respawn. Can be put on the floor, walls and fences. Red candles drop hearts and yellow candles drop gold.
* Destructible Walls - Blocks that serve as secret destructible walls to hold secrets, hit them once will break them. They can be overlayed with a different block texture by right clicking on it with the specific block.

Items:
* Whip - Whip that serves both as a weapon and grapple. Right click to grapple to a block. When swinging with the whip, W and S would swing you back and forth, holding space will raise you and holding shift will lower you. Press right click to let go. (There would 3 different whips, gradually progressing in power.
* Leather Whip - Basic Whip Steel Whip - Second Whip, more powerful. Morning Star - Third Whip, most powerful.
* Hearts - Ammo for subweapons, comes in small (worth 1) and big (worth 5). Would be dropped by red candles or enemies.
* Knife - Knife that's thrown straight forward, costs 1 heart. It can hit levers and buttons.
* Axe - Axe that's thrown in an arc, costs 2 hearts. It can it levers and buttons.
* Holy Water - Holy water thrown onto the ground, straight line of fire that travels for 3 seconds, costs 3 hearts.
* Boomerang Cross - Cross that's thrown straight forward and comes back, costs 2 hearts. It can pass through walls and hit levers and buttons.
* Bible - Bible that spins around the player for 5 seconds, costs 5 hearts.
* Stopwatch - Stopwatch that freezes time for 5 seconds, costs 10 hearts.
* Heart Refresh - Gives player 10 hearts, consumable, stacked to 64.
* Gold Coin - Dropped from yellow candles, stack to 64.


Upgrades:
* Health Upgrade - Gives the player an extra health heart.
* Heart Upgrade - Gives the player 10 extra heart ammo.


Ability Items:
* Jump Stone: The player can perform a higher floaty jump.
* Spider Stone - Allows the player to cling onto walls, as well as wall jump like in Mega Man X. Slowly slide down the wall if you don't wall jump.
* Mermaid Stone - Allows the player to breath underwater.
* Dash Stone - By pressing a button (customizable), the player can dash foward. Can also be performed mid-air.
* Leap Stone - Player can perform a double jump.
* Gryphon Stone - By pressing a button (customizable) the player can perform a super high leaping jump.


## Development
This mod uses ASM. Please enable `-Dfml.coreMods.load=com.zr2.castlevania.asm.CastlevaniaAsm` in the VM options.

## Disclaimer
Part the code of this mod is borrowed from yyon's Grapple Mod under GPLv3 license.